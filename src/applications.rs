use std::collections::LinkedList;

pub enum Filter {
    Identity,
}

pub enum TypesAndValues {}

pub enum OpsAndFuncs {}

pub enum Application {
    Applications(LinkedList<Application>),

    Filter(Filter),
    // TypesAndValues(TypesAndValues),
    // OpsAndFuncs(OpsAndFuncs)
}
