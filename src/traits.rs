use crate::applications::*;

pub trait DataFormat: Sized {
    fn apply(self: Self, application: Application) -> Self {
        match application {
            Application::Applications(applications) => applications
                .into_iter()
                .fold(self, |acc, curr| acc.apply(curr)),

            Application::Filter(op) => {
                self.filter(op)
            }
        }
    }

    fn filter(self: Self, filter: Filter) -> Self;
}
