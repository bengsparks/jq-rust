mod jsonimpl;

pub use jsonimpl::*;

use crate::traits::DataFormat;
use crate::applications::*;


impl DataFormat for serde_json::Value {
    fn filter(self: Self, filter: Filter) -> Self {
        match filter {
            Filter::Identity => self
        }
    }
}